// Copyright Epic Games, Inc. All Rights Reserved.

#include "hw21.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, hw21, "hw21" );
