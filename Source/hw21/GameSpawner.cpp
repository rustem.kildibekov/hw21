// Fill out your copyright notice in the Description page of Project Settings.


#include "GameSpawner.h"
#include "Food.h"
#include "SnakeBase.h"
#include "SpeedBonus.h"
#include "Block.h"

// Sets default values
AGameSpawner::AGameSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Hight = 10;
	Width = 10;
	CellSize = 100;

	

	
}

// Called when the game starts or when spawned
void AGameSpawner::BeginPlay()
{
	Super::BeginPlay();

	CenterShiftHight = Hight * CellSize / 2;
	CenterShiftWidth = Width * CellSize / 2;

	spawnFood();
	spawnBonus();
	buildBorders();

}

FVector AGameSpawner::CoordinateToLocation(Coordinate value)
{
	float x = value.x * CellSize;
	float y = value.y * CellSize;
	return GetActorLocation() + FVector(x - CenterShiftHight, y - CenterShiftWidth, 0);
}

Coordinate AGameSpawner::getRandomCoordinate()
{
	return Coordinate(FMath::RandRange(1, Hight-2), FMath::RandRange(1, Width-2));
}

void AGameSpawner::spawnBonus()
{
	if (SpeedBonuses.Num() == 0) return;

	UClass* object = SpeedBonuses[FMath::RandRange(0, SpeedBonuses.Num()-1)].Get();
	Spawn(object, CoordinateToLocation(getRandomCoordinate()));
}

void AGameSpawner::spawnFood()
{
	AActor* newObject = Spawn(Food.Get(), CoordinateToLocation(getRandomCoordinate()));
	newObject->OnDestroyed.AddDynamic(this, &AGameSpawner::respawnFood);
}

void AGameSpawner::respawnFood(AActor* DestroyedActor)
{
	spawnFood();
}

void AGameSpawner::respawnBonus(AActor* DestroyedActor)
{
	spawnBonus();
}

AActor* AGameSpawner::Spawn(UClass* object, FVector Location)
{	
	return GetWorld()->SpawnActor(object, &Location);
}

void AGameSpawner::buildBorders()
{
	UClass* object = Block.Get();
	//Spawn(object, CoordinateToLocation(Coordinate(0, 0)));
	for (int xi = 0; xi < Width; ++xi) {
		Spawn(object, CoordinateToLocation(Coordinate(xi, 0)));
		Spawn(object, CoordinateToLocation(Coordinate(xi, Hight - 1)));
	} 
	for (int yi = 1; yi < Hight-1; ++yi) {
		Spawn(object, CoordinateToLocation(Coordinate(0, yi)));
		Spawn(object, CoordinateToLocation(Coordinate(Width - 1, yi)));
	}


	
}

// Called every frame
void AGameSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

