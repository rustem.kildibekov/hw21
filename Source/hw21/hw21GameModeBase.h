// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "hw21GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HW21_API Ahw21GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
