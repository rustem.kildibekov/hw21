// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SpeedBonus.generated.h"

UCLASS()
class HW21_API ASpeedBonus : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpeedBonus();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	UPROPERTY(EditDefaultsOnly)
	float increaseSpeedValue;

	UPROPERTY(EditDefaultsOnly)
	float period;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactar, bool bIsHead) override;

};
