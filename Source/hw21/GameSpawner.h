// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameSpawner.generated.h"

class AFood;
class ASnakeBase;
class ASpeedBonus;
class ABlock;

struct Coordinate {
	int x;
	int y;
	Coordinate(int _x, int _y) {
		x = _x;
		y = _y;
	}
};

UCLASS()
class HW21_API AGameSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameSpawner();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	float CenterShiftHight;
	float CenterShiftWidth;

	FVector CoordinateToLocation(Coordinate value);
	Coordinate getRandomCoordinate();
	void spawnBonus();
	void spawnFood();

	UFUNCTION()
	void respawnFood(AActor* DestroyedActor);

	UFUNCTION()
	void respawnBonus(AActor* DestroyedActor);

	AActor* Spawn(UClass* object, FVector Location);

	void buildBorders();

public:	

	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "10", ClampMax = "20", UIMin = "10", UIMax = "20"))
	int Hight;

	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "10", ClampMax = "20", UIMin = "10", UIMax = "20"))
	int Width;

	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "100", ClampMax = "100", UIMin = "100", UIMax = "100"))
	int CellSize;


	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> Food;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> Snake;

	UPROPERTY(EditDefaultsOnly)
	TArray < TSubclassOf<ASpeedBonus> > SpeedBonuses;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABlock> Block;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
