// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::UP;
	MovementSpeed = 20.f;
}
// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) {
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(GetActorLocation() - NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 elementIndex = SnakeElements.Add(NewSnakeElement);
		if (elementIndex == 0) {
			NewSnakeElement->SetFirstElementType();
			
		}
		
	}
}

void ASnakeBase::ChangeSpeed(float increase)
{
	UE_LOG(LogTemp, Log, TEXT("Change speed by %f"),  increase);
	MovementSpeed -= MovementSpeed * increase / 100;
	SetActorTickInterval(MovementSpeed);

	UE_LOG(LogTemp, Log, TEXT("Speed by %f"), MovementSpeed);
}

void ASnakeBase::TempChangeSpeed(float increase, float period)
{
	float lastSpeed = GetActorTickInterval();
	ChangeSpeed(increase);
	FTimerHandle handle;
	GetWorld()->GetTimerManager().SetTimer(handle, [this, lastSpeed]() {
			SetActorTickInterval(lastSpeed);
		},
		period, false);

}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MovementStep = ElementSize;

	switch (LastMoveDirection) {
	case EMovementDirection::UP: 
		MovementVector.X += MovementStep;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementStep;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= MovementStep;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += MovementStep;
		break;

	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();


	for (int i = SnakeElements.Num() - 1; i > 0; --i) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

